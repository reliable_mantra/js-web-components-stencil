import { Component, State } from "@stencil/core";

import { AV_API_KEY } from '../../global/global';

@Component({
    tag: 'my-stock-finder',
    styleUrl: './stock-finder.css',
    shadow: true
})
export class StockFinder {
    stockNameInput: HTMLInputElement;

    @State() searchResults: {symbol: string, name: string}[];

    onFindStocks(event: Event) {
        event.preventDefault();
        const stockName = this.stockNameInput.value;
        fetch(`https://www.alphavantage.co/query?function=SYMBOL_SEARCH&keywords=${stockName}&apikey=${AV_API_KEY}`)
            .then(res => res.json())
            .then(parsedRes => {
                console.log(parsedRes);
            })
            .catch(err => {
                console.log(err);
            });
    }

    render() {
        return [
            <form onSubmit={this.onFindStocks.bind(this)}>
                <input 
                    type="text" 
                    id="stock-symbol" 
                    ref={el => this.stockNameInput = el} />
                <button type="submit">Find!</button>
            </form>,

        ];
    }
}

